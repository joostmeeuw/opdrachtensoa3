import com.cinema.*;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

public class CalculatePriceTests {

    // start student.

    // amount of tickets and weekday or no weekday are irrelevant for students

    // student (premium, weekday, less than six tickets)
    @Test
    public void studentWithPremiumTicketsOnAWeekdayWithLessThanSixTicketsShouldGiveSecondTicketFreeAndAskPremiumPrice() {
        //Arrange
        Movie movie = new Movie("Winni de Poo");

        MovieScreening movieScreening = new MovieScreening(LocalDateTime.now(), 5.00, movie);
        MovieTicket movieTicket = new MovieTicket(10, 12, true, movieScreening);

        Order order = new Order(1234, true);
        order.addSeatReservation(movieTicket);
        order.addSeatReservation(movieTicket);
        order.addSeatReservation(movieTicket);
        order.addSeatReservation(movieTicket);

        //Act
        double price = order.calculatePrice();
        //Assert
        assertEquals(14.00, price, 0.0);

    }
    // student (non premium, weekday, less than six tickets)
    @Test
    public void studentWithNoPremiumTicketsOnAWeekdayWithLessThanSixTicketsShouldGiveSecondTicketFreeAndNotChargePremium() {
        //Arrange
        Movie movie = new Movie("Winni de Poo");

        MovieScreening movieScreening = new MovieScreening(LocalDateTime.now(), 5.00, movie);
        MovieTicket movieTicket = new MovieTicket(10, 12, false, movieScreening);

        Order order = new Order(1234, true);
        order.addSeatReservation(movieTicket);
        order.addSeatReservation(movieTicket);
        order.addSeatReservation(movieTicket);
        order.addSeatReservation(movieTicket);

        //Act
        double price = order.calculatePrice();
        //Assert
        assertEquals(10.00, price, 0.0);

    }

    // QUESTION: Wat te doen met paden die mogelijkheden veroorzaken die niet passen de voorwaarden van het algoritme?
    // student (premium, weekday, more than six tickets)
    // student (non premium, weekday, more than six tickets)

    // end student

    // start normal customer (premium, weekday, less than six tickets)

    // normal customer (premium, weekday, less than six tickets)
    @Test
    public void customerWithPremiumTicketsOnWeekDayWithLessThanSixTicketsShouldGiveSecondTicketFreeAndChargePremium() {
        //Arrange
        Movie movie = new Movie("Winni de Poo");

        String str = "2022-02-03 12:30";
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm");
        LocalDateTime dateTime = LocalDateTime.parse(str, formatter);

        MovieScreening movieScreening = new MovieScreening(dateTime, 5.00, movie);
        MovieTicket movieTicket = new MovieTicket(10, 12, true, movieScreening);

        Order order = new Order(1234, false);
        order.addSeatReservation(movieTicket);
        order.addSeatReservation(movieTicket);
        order.addSeatReservation(movieTicket);
        order.addSeatReservation(movieTicket);

        //Act
        double price = order.calculatePrice();
        //Assert
        assertEquals(16.00, price, 0.0);

    }
    // normal customer (non premium, weekday, less than six tickets)
    @Test
    public void customerNoPremiumOnWeekDayWithLessThanSixTicketsShouldGiveSecondTicketFreeAndChargeNoPremium() {
        //Arrange
        Movie movie = new Movie("Winni de Poo");

        String str = "2021-02-03 12:30";
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm");
        LocalDateTime dateTime = LocalDateTime.parse(str, formatter);

        MovieScreening movieScreening = new MovieScreening(dateTime, 5.00, movie);
        MovieTicket movieTicket = new MovieTicket(10, 12, false, movieScreening);

        Order order = new Order(1234, false);
        order.addSeatReservation(movieTicket);
        order.addSeatReservation(movieTicket);
        order.addSeatReservation(movieTicket);
        order.addSeatReservation(movieTicket);

        //Act
        double price = order.calculatePrice();
        //Assert
        assertEquals(10.00, price, 0.0);

    }

    // normal customer (premium, non weekday, less than six tickets)
    @Test
    public void customerWithPremiumOnWeekDayWithLessThanSixTicketsShouldGiveNoDiscountAndChargePremium() {
        //Arrange
        Movie movie = new Movie("Winni de Poo");

        String str = "2022-02-04 12:30";
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm");
        LocalDateTime dateTime = LocalDateTime.parse(str, formatter);

        MovieScreening movieScreening = new MovieScreening(dateTime, 5.00, movie);
        MovieTicket movieTicket = new MovieTicket(10, 12, true, movieScreening);

        Order order = new Order(1234, false);
        order.addSeatReservation(movieTicket);
        order.addSeatReservation(movieTicket);
        order.addSeatReservation(movieTicket);
        order.addSeatReservation(movieTicket);

        //Act
        double price = order.calculatePrice();
        //Assert
        assertEquals(32.00, price, 0.0);

    }

    // normal customer (non premium, non weekday, less than six tickets)
    @Test
    public void customerWithNoPremiumOnNoWeekDayWithLessThanSixTicketsShouldNotGetSecondTicketFreeAndNoPremiumPrice() {
        //Arrange
        Movie movie = new Movie("Winni de Poo");

        String str = "2022-02-04 12:30";
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm");
        LocalDateTime dateTime = LocalDateTime.parse(str, formatter);

        MovieScreening movieScreening = new MovieScreening(dateTime, 5.00, movie);
        MovieTicket movieTicket = new MovieTicket(10, 12, false, movieScreening);

        Order order = new Order(1234, false);
        order.addSeatReservation(movieTicket);
        order.addSeatReservation(movieTicket);
        order.addSeatReservation(movieTicket);
        order.addSeatReservation(movieTicket);

        //Act
        double price = order.calculatePrice();
        //Assert
        assertEquals(20.00, price, 0.0);

    }

    // normal customer (premium, weekday, more than six tickets)
    @Test
    public void customerWithPremiumOnAWeekDayWithMoreThanSixTicketsShouldNotGiveDiscountAndChargePremiumAndGiveSecondTicketFree() {
        //Arrange
        Movie movie = new Movie("Winni de Poo");

        String str = "2022-02-03 12:30";
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm");
        LocalDateTime dateTime = LocalDateTime.parse(str, formatter);

        MovieScreening movieScreening = new MovieScreening(dateTime, 5.00, movie);
        MovieTicket movieTicket = new MovieTicket(10, 12, true, movieScreening);

        Order order = new Order(1234, false);
        order.addSeatReservation(movieTicket);
        order.addSeatReservation(movieTicket);
        order.addSeatReservation(movieTicket);
        order.addSeatReservation(movieTicket);
        order.addSeatReservation(movieTicket);
        order.addSeatReservation(movieTicket);
        order.addSeatReservation(movieTicket);
        order.addSeatReservation(movieTicket);


        //Act
        double price = order.calculatePrice();
        //Assert
        assertEquals(32.00, price, 0.0);

    }

    // normal customer (non premium, weekday, more than six tickets)
    @Test
    public void customerWithNonPremiumWithMoreThanSixTicketsOnWeekDayShouldNotGiveDiscountAndNotChargePremiumAndGiveSecondTicketFree() {
        //Arrange
        Movie movie = new Movie("Winni de Poo");

        String str = "2022-02-03 12:30";
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm");
        LocalDateTime dateTime = LocalDateTime.parse(str, formatter);

        MovieScreening movieScreening = new MovieScreening(dateTime, 5.00, movie);
        MovieTicket movieTicket = new MovieTicket(10, 12, false, movieScreening);

        Order order = new Order(1234, false);
        order.addSeatReservation(movieTicket);
        order.addSeatReservation(movieTicket);
        order.addSeatReservation(movieTicket);
        order.addSeatReservation(movieTicket);
        order.addSeatReservation(movieTicket);
        order.addSeatReservation(movieTicket);
        order.addSeatReservation(movieTicket);
        order.addSeatReservation(movieTicket);


        //Act
        double price = order.calculatePrice();
        //Assert
        assertEquals(20.00, price, 0.0);

    }

    // normal customer (premium, non weekday, more than six tickets)
    @Test
    public void customerWithPremiumOmNoWeekDayWithMoreThanSixTicketsShouldGiveDiscountAndChargePremium() {
        //Arrange
        Movie movie = new Movie("Winni de Poo");

        String str = "2022-02-04 12:30";
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm");
        LocalDateTime dateTime = LocalDateTime.parse(str, formatter);

        MovieScreening movieScreening = new MovieScreening(dateTime, 5.00, movie);
        MovieTicket movieTicket = new MovieTicket(10, 12, true, movieScreening);

        Order order = new Order(1234, false);
        order.addSeatReservation(movieTicket);
        order.addSeatReservation(movieTicket);
        order.addSeatReservation(movieTicket);
        order.addSeatReservation(movieTicket);
        order.addSeatReservation(movieTicket);
        order.addSeatReservation(movieTicket);
        order.addSeatReservation(movieTicket);
        order.addSeatReservation(movieTicket);


        //Act
        double price = order.calculatePrice();
        //Assert
        assertEquals(57.6, price, 0.0);

    }

    // normal customer (non premium, non weekday, more than six tickets)
    @Test
    public void customerWithNoPremiumOmNoWeekDayWithMoreThanSixTicketsShouldGiveDiscountAndChargeNoPremium() {
        //Arrange
        Movie movie = new Movie("Winni de Poo");

        String str = "2022-02-04 12:30";
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm");
        LocalDateTime dateTime = LocalDateTime.parse(str, formatter);

        MovieScreening movieScreening = new MovieScreening(dateTime, 5.00, movie);
        MovieTicket movieTicket = new MovieTicket(10, 12, false, movieScreening);

        Order order = new Order(1234, false);
        order.addSeatReservation(movieTicket);
        order.addSeatReservation(movieTicket);
        order.addSeatReservation(movieTicket);
        order.addSeatReservation(movieTicket);
        order.addSeatReservation(movieTicket);
        order.addSeatReservation(movieTicket);
        order.addSeatReservation(movieTicket);
        order.addSeatReservation(movieTicket);


        //Act
        double price = order.calculatePrice();
        //Assert
        assertEquals(36, price, 0.0);

    }

    // end normal customer

}
