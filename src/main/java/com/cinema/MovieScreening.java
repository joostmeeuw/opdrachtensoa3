package com.cinema;

import java.text.DateFormat;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;

public class MovieScreening {
    private LocalDateTime dateAndTime;
    private Double pricePerSeat;
    private Movie movie;
    private ArrayList<MovieTicket> tickets;

    public MovieScreening(LocalDateTime dateAndTime, Double pricePerSeat, com.cinema.Movie movie) {
        this.dateAndTime = dateAndTime;
        this.pricePerSeat = pricePerSeat;
        this.movie = movie;
        this.tickets = new ArrayList<>();
    }

    public Double getPricePerSeat() {
        return pricePerSeat;
    }

    public LocalDateTime getDateAndTime() {
        return dateAndTime;
    }

    @Override
    public String toString() {

        return "Screening " +
                "date=" + dateAndTime.format(DateTimeFormatter.ofPattern("yyyy-MM-dd"))+
                ", pricePerSeat=" + pricePerSeat + " | Movie= " + this.movie;
    }
}
