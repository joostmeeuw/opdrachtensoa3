package com.cinema;

public class MovieTicket {
    private int rowNr;
    private int seatNr;
    private boolean isPremium;
    private MovieScreening screening;

    public MovieTicket(int rowNr, int seatNr, boolean isPremium, MovieScreening screening) {
        this.rowNr = rowNr;
        this.seatNr = seatNr;
        this.isPremium = isPremium;
        this.screening = screening;
    }

    public MovieScreening getScreening() {
        return screening;
    }

    public boolean isPremiumTicket() {
        return isPremium;
    }

    public double getPrice() {
        return screening.getPricePerSeat();
    }

    @Override
    public String toString() {
        return "Ticket " +
                "row number=" + rowNr +
                ", seat number=" + seatNr +
                ", premium ticket=" + isPremium +  " | " +
                screening.toString();
    }
}
