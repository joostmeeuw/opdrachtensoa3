package com.cinema.calculatestrategy;

import com.cinema.MovieTicket;

import java.util.ArrayList;

public interface CalculateStrategy {
    double calculatePrice(ArrayList<MovieTicket> tickets);
}
