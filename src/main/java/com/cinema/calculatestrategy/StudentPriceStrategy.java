package com.cinema.calculatestrategy;

import com.cinema.MovieTicket;

import java.util.ArrayList;

public class StudentPriceStrategy implements CalculateStrategy {

    public double calculatePrice(ArrayList<MovieTicket> tickets) {
        double price = 0.00;
        int ticketNumber = 0;

        for (MovieTicket ticket : tickets) {

            ticketNumber++;

            double pricePerTicket = ticket.getPrice();

            if (ticket.isPremiumTicket()) {
                    pricePerTicket += 2.00;
            }

            if(ticketNumber == 2){
                ticketNumber = 0;
                continue;
            }

            price += pricePerTicket;
        }

        return price;
    }

}
