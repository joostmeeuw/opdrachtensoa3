package com.cinema.calculatestrategy;

import com.cinema.MovieTicket;

import java.util.ArrayList;

public class StandardPriceStrategy implements CalculateStrategy {

    public double calculatePrice(ArrayList<MovieTicket> tickets) {
        double price = 0.00;
        boolean isWeekDay = false;
        int ticketNumber = 0;

        for (MovieTicket ticket : tickets) {

            ticketNumber++;

            double pricePerTicket = ticket.getPrice();

            String dayOfTheWeek = ticket.getScreening().getDateAndTime().getDayOfWeek().toString();

            if (!dayOfTheWeek.equals("FRIDAY") && !dayOfTheWeek.equals("SATURDAY") && !dayOfTheWeek.equals("SUNDAY")) {
                isWeekDay = true;
            }

            if (ticket.isPremiumTicket()) {
                    pricePerTicket += 3.00;
            }

            if(ticketNumber == 2 && isWeekDay){
                ticketNumber = 0;
                continue;
            }

            price += pricePerTicket;
        }

        if (!isWeekDay && tickets.size() >= 6) {
            price = price * 0.9;
        }

        return price;
    }

}
