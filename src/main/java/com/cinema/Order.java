package com.cinema;

import java.util.ArrayList;

import com.cinema.calculatestrategy.CalculateStrategy;
import com.cinema.calculatestrategy.StandardPriceStrategy;
import com.cinema.calculatestrategy.StudentPriceStrategy;
import com.cinema.ticketexportstrategy.JsonExportStrategy;
import com.cinema.ticketexportstrategy.PlainTextExportStrategy;
import com.cinema.ticketexportstrategy.TicketExportFormat;
import com.cinema.ticketexportstrategy.TicketExportStrategy;


public class Order {
    private int orderNr;
    private boolean isStudentOrder;
    private ArrayList<MovieTicket> tickets;
    private CalculateStrategy calculateStrategy;
    private TicketExportStrategy ticketExportStrategy;

    public Order(int orderNr, boolean isStudentOrder) {
        this.orderNr = orderNr;
        this.isStudentOrder = isStudentOrder;
        this.tickets = new ArrayList<>();
    }

    public void setCalculateStrategy(CalculateStrategy calculateStrategy) {
        this.calculateStrategy = calculateStrategy;
    }

    public void setTicketExportStrategy(TicketExportStrategy ticketExportStrategy) {
        this.ticketExportStrategy = ticketExportStrategy;
    }

    public int getOrderNr() {
        return orderNr;
    }

    public void addSeatReservation(MovieTicket ticket) {
        tickets.add(ticket);
    }

    public double calculatePrice() {

        if(isStudentOrder){
            setCalculateStrategy(new StudentPriceStrategy());
        } else{
            setCalculateStrategy(new StandardPriceStrategy());
        }

        return this.calculateStrategy.calculatePrice(tickets);
    }

    @Override
    public String toString() {

        String ticketsString = "\n";

        for (MovieTicket ticket: this.tickets
             ) {
            ticketsString += ticket.toString() + "\n";
        }



        return "Order:" + orderNr +
                ", isStudentOrder " + isStudentOrder +
                ", tickets:" + ticketsString;
    }

    public void export(TicketExportFormat ticketExportFormat){

        if(ticketExportFormat == TicketExportFormat.JSON) {
            setTicketExportStrategy(new JsonExportStrategy());
        }

        if(ticketExportFormat == TicketExportFormat.PLAINTEXT) {
            setTicketExportStrategy(new PlainTextExportStrategy());
        }

        ticketExportStrategy.export(this);
    }

}
