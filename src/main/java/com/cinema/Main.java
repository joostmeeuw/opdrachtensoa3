package com.cinema;

import com.cinema.ticketexportstrategy.TicketExportFormat;

import java.time.LocalDateTime;

public class Main {

    public static void main(String[] args) {
	// write your code here
        Movie movie = new Movie("Winni de Poo");

        MovieScreening movieScreening = new MovieScreening(LocalDateTime.now(), 4.00, movie);

        MovieTicket movieTicket = new MovieTicket(10, 12, false, movieScreening);

        Order order = new Order(1234, false);
        order.addSeatReservation(movieTicket);
        order.addSeatReservation(movieTicket);
        order.addSeatReservation(movieTicket);
        order.addSeatReservation(movieTicket);

        order.export(TicketExportFormat.PLAINTEXT);
        order.export(TicketExportFormat.JSON);

    }
}
