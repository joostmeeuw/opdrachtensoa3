package com.cinema.ticketexportstrategy;

public enum TicketExportFormat {
    PLAINTEXT,
    JSON
}
