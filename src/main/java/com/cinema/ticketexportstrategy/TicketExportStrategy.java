package com.cinema.ticketexportstrategy;

import com.cinema.Order;

public interface TicketExportStrategy {
    void export(Order order);
}
