package com.cinema.ticketexportstrategy;

import com.cinema.Order;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

public class JsonExportStrategy implements TicketExportStrategy {
    public void export(Order order) {
        GsonBuilder builder = new GsonBuilder();
        Gson gson = builder.create();
        String json = gson.toJson(order);
        System.out.println(json);
    }
}
