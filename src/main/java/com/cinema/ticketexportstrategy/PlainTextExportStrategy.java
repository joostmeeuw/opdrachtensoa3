package com.cinema.ticketexportstrategy;

import com.cinema.Order;

public class PlainTextExportStrategy implements TicketExportStrategy {
    public void export(Order order) {
        System.out.println(order.toString());
    }
}
