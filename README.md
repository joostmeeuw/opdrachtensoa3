# Bioscoop casus
Onze huiswerk opdrachten 

## Description
Hier zijn in verschillende branches ons huiswerk te vinden

## History

* week 1
    * (les 1) Op branch week-1-opdracht-1 de basis gemaakt en de twee functies
    * (les 1) Op deze google link is de opdracht uit de les te vinden [Opdracht Non-Functionals](https://docs.google.com/document/d/189ThM1hMChSFEsmkgqqZ0UDGOtjjVFCLxu9zy0_FqZk/edit?usp=sharing)
    * (les 2) Hier is de afbeelding te zien omtrent de opdrachten van CC: ![Afbeedling](./images/Graph%20diagram%20SOFA3%20(calculatePrice).png)   
    * (les 2) Testen gemaakt, veel getwijfeld, uiteindelijk gekozen voor de meest logische paden. Graag in de les hier meer feedback over of voorbeelden van andere, zowel voor de tests als voor de graph modellatie.
* week 2
    * (les 3) Het toepassen van een design patern
    * (les 2) Hier is het klassendiagram te zien voor de refactoring van het Strategy Pattern: ![Afbeedling](./images/UML%20Klassendiagram%20SOFA3%20bioscoop-casus%20-%20Pagina 1.png)   